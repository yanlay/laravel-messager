<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function messages()
    {
        return $this->morphMany(Message::class, 'sendable');
    }

    public function conversations()
    {
        return $this->belongsToMany(Conversation::class, 'participant');
    }

    public function scopeGetConversationByUser($query, $user_id)
    {
        return $query->with(['conversations' => function ($query) use (&$user_id) {
                    $query->with(['users' => function ($query) {
                        $query->where('users.id', '<>', auth()->user()->id);
                    }])->whereHas('users', function ($query) use (&$user_id) {
                    $query->where('users.id', $user_id);
                })->has('users', '=', 2)->limit(1);
            }])->where('id', Auth::user()->id);
    }
}
