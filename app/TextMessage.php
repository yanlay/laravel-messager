<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Message;
use Auth;

class TextMessage extends Model implements \App\Services\Message\MessageInterface
{
    protected $fillable = ['content'];

    public function message()
    {
        return $this->morphOne(Message::class, 'sendable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store($data)
    {
        $message = new Message();
        $text = new TextMessage($data['message']);
        $text->save();
        $message->sendable()->associate($text);
        $message->chatable()->associate(Auth::user());
        return $message;
    }
}
