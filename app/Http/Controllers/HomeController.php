<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Conversation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::where('id', '<>', Auth::user()->id)->get();

        $conversations = Auth::user()->conversations()->with([
            'users' => function ($query) {
                $query->where('user_id', '<>', auth()->user()->id);
            },

            'messages' => function ($query) {
                $query->with('sendable')->latest();
            }

        ])->get();

        return view('home')->with(['users' => $users, 'conversations' => $conversations]);
    }
}
