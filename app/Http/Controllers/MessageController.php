<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conversation;
use App\Services\MessageService;

class MessageController extends Controller
{
    public $service;

    public function __construct(MessageService $service)
    {
        $this->service = $service;
    }


    public function conversationCreateOrGet(Request $request)
    {
        $conversation = $this->service->getConversation($request->all());

        return response()->json($conversation);
    }

    public function saveMessage(Request $request)
    {
        $conversation = $this->service->saveMessage($request->all());

        return response()->json($conversation);
    }

    /**
     * Get Messages for giving conversations id
     */

    public function getMessages(Conversation $conversation)
    {
        $conversation = $conversation->load('messages.sendable', 'messages.chatable');
        $collection = $conversation->messages->transform(function ($message) {
            return [
                'conversation_id' => $message->conversation_id,
                'sender_id' => $message->chatable_id,
                'sender_name' => $message->chatable->name,
                'message' => $message->sendable,
                'message_type' => $message->sendable_type
            ];
        });
        return response()->json($collection->toArray());
    }
}
