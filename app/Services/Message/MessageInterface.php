<?php
namespace App\Services\Message;

interface MessageInterface
{
    public function store($data);
}
