<?php
namespace App\Services\Message;

use Illuminate\Database\Eloquent\Model;

class Message
{
    protected $message;

    public function __construct(MessageInterface $message)
    {
        $this->message = $message;
    }

    /**
     * run save method giving object
     *
     * @param array $data
     *
     * @return Model
     */

    public function save($data)
    {
        return $this->message->store($data);
    }
}
