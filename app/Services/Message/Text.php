<?php
namespace App\Services\Message;

use App\Message;
use App\TextMessage;
use Auth;

class Text implements MessageInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function save($data)
    {
        $message = new Message();
        $text = new TextMessage($data['message']);
        $message->sendable()->associate($text);
        $message->chatable()->associate(Auth::user());

        return $message;
    }
}
