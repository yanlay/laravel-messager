<?php

namespace App\Services;

use Auth;
use App\User;
use App\Conversation;
use App\Services\Message\Photo;
use App\Services\Message\Pet;
use App\Services\Message\Message;
use App\Services\Message\Text;
use App\TextMessage;
use App\Notifications\NewMessageNotification;

class MessageService
{
    public function conversationCreateOrGet($data)
    {

        $conversation = $this->getConversationByUser($data['user_id']);

        if (!$conversation) {
            $conversation = $this->createConversation($data['user_id']);
        }

        return response()->json($conversation);
    }

    public function getConversationByUser($user_id)
    {
        $user = User::getConversationByUser($user_id)->first();

        if (sizeof($user->conversations) !== 0) {
            return $user->conversations[0];
        }

        return null;
    }

    public function createConversation($user_id)
    {
        $conversation = Conversation::create([]);

        $conversation->users()->sync([
            Auth::user()->id => ['status' => 0],
            $user_id => ['status' => 0]
        ]);

        $conversation->load(['users' => function ($query) {
            $query->where('users.id', '<>', auth()->user()->id);
        }]);

        return $conversation;
    }

    public function saveMessage($data)
    {
        $conversation = Conversation::with(['users' => function ($query) {
            return $query->where('user_id', '<>', auth()->user()->id);
        }])->where('id', $data['conversation_id'])->first();

        if ($data['type'] == 'text_message') {
            $message = new Message(new TextMessage());
        }

        if ($data['type'] == 'photo') {
            $message = new Message(new Photo());
        }

        if ($data['type'] == 'pet') {
            $message = new Message(new Pet());
        }

        $message = $message->save($data);

        $conversation->messages()->save($message);

        $conversation->users->each(function ($user) use (&$conversation) {
            $user->notify(new NewMessageNotification($conversation));
        });


        return $conversation;

    }
}
