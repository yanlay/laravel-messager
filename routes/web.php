<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/conversation/{conversation}/messages', 'MessageController@getMessages');

Route::post('/conversation/create', 'MessageController@conversationCreateOrGet');

Route::post('/send/message', 'MessageController@saveMessage');
