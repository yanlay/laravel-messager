@extends('layouts.app')

@section('content')
<div class="container">
    <message-view :users="{{ json_encode($users) }}" :conversations= "{{ json_encode($conversations) }}" :user_id= "{{ Auth::user()->id }}"></message-view>
</div>
@endsection
